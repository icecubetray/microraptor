> This project is still a major work in progress.

# Microraptor
Prototype YAML-based, multi-threaded build system.

## Development
Will be limited, as I have little free time to spare.

#### Branching
Branch | Type | Description
------ | ---- | -----------
master | alpha | Contains the latest release fresh out of development (alpha-ish), so currently nothing but repository files
dev | development | Main development branch
feature/_*name*_ | development | Contains a feature
flavor/_*name*_ | development | Contains a configuration flavor
